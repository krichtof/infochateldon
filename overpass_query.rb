require 'overpass_api_ruby'

options={:bbox => {:s => 45.937303212463,
                   :n => 46.024621295988,
                   :w => 3.4560585021973,
                   :e => 3.6167335510254},
         :timeout => 900,
         :maxsize => 1073741824}
overpass = OverpassAPI::QL.new(options)

query = "node['shop'~'.*'];(._;>;);out body;"

response = overpass.query(query)
File.open("shops.json", "w") {|f| f.write response.to_json}
